package com.example.newprofile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_second.*
import com.bumptech.glide.Glide
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_first.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        glide()
        init()

    }
    private fun glide(){
        Glide.with(this).load(("https://www.arlingtonlibrary.org/sites/default/files/Summer-Reading-Club-2018-front-03-300x200_0.png")).into(image2)
        Glide.with(this).load(("https://media.istockphoto.com/vectors/vector-logo-concept-of-a-book-club-with-people-reading-vector-id1092225920?k=6&m=1092225920&s=612x612&w=0&h=LJOm05UpZtl_j0RUtxvaY29XUCEiEuE3GqepulyhejY=")).into(image1)
    }
    private fun init(){
        emailTextView.text = intent.extras!!.getString("email", "")
        passwordTextView.text = intent.extras!!.getString("password", "")

        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("user")



        // Read from the database
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.getValue(UserModel::class.java)
                Surname.text = value!!.lastName
                Name.text = value.firstName
                Age.text = value.age.toString()
                


                Log.d("", "Value is: $value")



            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("", "Failed to read value.", error.toException())
            }
        })


    }

}


package com.example.newprofile

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
        auth = FirebaseAuth.getInstance()
    }

    private fun init() {
        readData()
        LogInButton.setOnClickListener {
            signIn()
            saveData()
            intent.putExtra("email", LogInEdit.text.toString())
            intent.putExtra("passord", PasswordEdit.text.toString())
        }
        SignUpButton.setOnClickListener {
            val intent = Intent (this, SignUpActivity::class.java).apply {

            }
            startActivity(intent)
        }

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
    }

    private fun signIn() {
        if (LogInEdit.text.isEmpty() && PasswordEdit.text.isEmpty()) {
            Toast.makeText(applicationContext, "Please fill all fields", Toast.LENGTH_SHORT)
                .show();
        } else
        auth.signInWithEmailAndPassword(LogInEdit.text.toString(), PasswordEdit.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("success", "signInWithEmail:success")

                    val intent = Intent(this, SecondActivity::class.java).apply {}
                    intent.putExtra("email", LogInEdit.text.toString())
                    intent.putExtra("password", PasswordEdit.text.toString())
                    startActivity(intent)

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("fail", "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                // ...
            }


    }
    private fun saveData(){
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        val edit = sharedPreferences.edit()
        edit.putString("email", LogInEdit.text.toString())
        edit.putString("password", PasswordEdit.text.toString())
        edit.commit()
    }
    private fun readData(){
        val sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE)
        LogInEdit.setText(sharedPreferences.getString("email", ""))
        PasswordEdit.setText(sharedPreferences.getString("password", ""))

    }
}

